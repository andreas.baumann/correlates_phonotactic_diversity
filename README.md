# Correlates in the evolution of phonotactic diversity

This project contains code for a diachronic analysis of correlates of diversity measures in English phonotactics. 

Cite as: *Baumann, A.; Matzinger, T. (in review). Correlates in the evolution of phonotactic diversity in English: linguistic structure, demographics, and network characteristics.*

A functional RStudio cloud project with the present data and code can be found here: https://rstudio.cloud/project/1481053

## Abstract 

There is an ongoing debate as to whether linguistic structure is influenced by demographic factors. Relationships between these two domains have been investigated on the phonological, morphological and lexical level, mainly drawing on synchronic data and comparative methodology. In this exploratory study, by contrast, we focus on the lesser recognized level of phonotactics, and adopt a methodologically orthogonal approach. We investigate the diachronic development of a single lineage, namely English, and compare it with concomitant developments of the demography of the English-speaking population. In addition to linguistic and demographic factors, we also derive characteristics of the underlying speaker network (network diameter; clustering coefficient). Empirically, we focus on the system of English consonant clusters, which we argue to be particularly sensitive to linguistic change so that effects of demography are expected to be more clearly visible than in more robust linguistic subsystems (e.g. phoneme inventory; morphology). By employing time-series clustering, it is shown that the trajectory of phonotactic diversity in English coda clusters most closely matches that of covariates related with density and heterogeneity of the speaker population. Linguistic covariates are less closely related. We conclude that heterogeneity of the linguistic input and the number of informants a learner is exposed to are potentially relevant factors  in the evolution of phonotactic diversity. 

## Setup

Clone project with ```git clone https://gitlab.com/andreas.baumann/correlates_phonotactic_diversity.git```. 
Alternatively, do setup as RStudio project (clone directly with RStudio: *New Project* > *Version Control* > *Git* > *Repository URL*: ```https://gitlab.com/andreas.baumann/correlates_phonotactic_diversity.git```). 

This project uses R together with ```renv``` dependency management (see https://rstudio.github.io/renv/articles/renv.html for an introduction). 

Alternatively, if you are using RStudio cloud, add https://rstudio.cloud/project/1481053 to your workspace.

## Usage

To compute all diversity measures as described in section 2.1 in the paper, run ```RScript preprocessing.R``` in the terminal.

To perform the cluster analysis described in section 3 in the paper, run ```RScript analysis.R``` in the terminal. This will generate all tables referred to in the paper as well as additional diagnostic measures and plots.

An alternative analysis (without time-series modeling with GAMs) can be found in the script ```analysis_alternative.R```.

## Acknowledgements

Many thanks to Timo Roettger, Niki Ritt and two anonymous reviewers for numerous helpful comments on our analysis. This research was funded by Austrian Science Fund (FWF), grant No. P27592-G18, and conducted as part of the project "Evolution of consonant clusters in English" (https://ecce.univie.ac.at/).
